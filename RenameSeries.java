public class RenameSeries 
{
    public static String getNouveauNom (String nomSerie, int saison, int episode)
    {
        StringBuilder nouveauNom = new StringBuilder(nomSerie + " " + saison + "x") ; 

        String strEpisode ; 

        if (episode < 10)
        {
            strEpisode = String.format("%02d",episode) ;
        }

        else 
        {
            strEpisode = Integer.toString(episode) ;
        }

        nouveauNom.append(strEpisode) ;
        
        return nouveauNom.toString().replace(" ", "_") ;
    }
}